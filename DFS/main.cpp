/* 
 * File:   main.cpp
 * Author: pratham
 *
 * Created on November 19, 2011, 12:53 AM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */

struct node {
    struct node *next, *pie;
    int st, et, color, vertex;

};

int n, time = 0;

struct node* make(int x) {
    struct node *temp;
    temp = (struct node *) malloc(sizeof (struct node));
    temp->vertex = x;
    temp->next = NULL;
    temp->color = 0;
    temp->pie = NULL;
    return temp;
}

void show(struct node *temp) {
    while (temp != NULL) {
        printf("%d\t", temp->vertex);
        temp = temp->next;
    }
}

void DFSV(struct node *u, struct node *head[]){
    struct node *v;
    v = u->next;
    time = time + 1;
    u->st = time;
    u->color = 1;
    while(v != NULL){
        if(head[v->vertex]->color == 0){
            head[v->vertex]->pie = u;
            DFSV(head[v->vertex],head);
        }
        v = v->next;
    }
    u->color = 2;
    time = time + 1;
    u->et = time;
}

int main(int argc, char** argv) {
    int i, j, v1, v2;
    printf("Enter Number of Vertices\n");
    scanf("%d", &n);
    struct node *head[n], *temp, *temp2;
    for (i = 1; i <= n; i++) {
        head[i] = make(i);
    }
    do {
        printf("Enter 1:To add edge, 2:Show Adjacency-List, 3:DFS, 4:show DFS, 0:Exit \n");
        scanf("%d", &j);
        switch (j) {
            case 1:
                printf("Enter Vertices between which Edge is to be created\n");
                scanf("%d %d", &v1, &v2);
                if (v1 > n || v2 > n)
                    printf("Error:There are less number of vertices");
                else {
                    temp = head[v1];
                    while (temp->next != NULL) {
                        temp = temp->next;
                    }
                    temp->next = make(v2);
                    if (v1 != v2) {
                        temp = head[v2];
                        while (temp->next != NULL) {
                            temp = temp->next;
                        }
                        temp->next = make(v1);
                    }
                }
                break;
            case 2:
                for (i = 1; i <= n; i++) {
                    show(head[i]);
                    printf("\n");
                }
                break;
            case 3:
                for (i = 1;i<=n; i++) {
                    if(head[i]->color == 0)
                        DFSV(head[i],head);
                }
                break;
            case 4:
                for (i=1;i<=n;i++) {
                    printf("Vertex = %d, ",i);
                    printf("Start Time = %d, ",head[i]->st);
                    printf("End Time = %d, ",head[i]->et);
                    if (head[i]->pie != NULL)
                        printf("Parent = %d\n",head[i]->pie->vertex);
                    else
                        printf("Parent is NULL\n");
                }
                break;
            case 0:
                break;
            default:
                printf("Invalid Selection\n");
        }
    } while (j != 0);
    return 0;
}

