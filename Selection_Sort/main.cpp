/* 
 * File:   main.cpp-Selection_Sort
 * Author: pratham
 *
 * Created on 19 August, 2011, 2:58 AM
 */

#include <stdlib.h>
#include <stdio.h>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int i,j,n;
    printf("Enter number of elements in Array:\n");
    scanf("%d",&n);
    int A[n];
    for(i=0;i<n;i++){
        printf("A[%d]=",i);
        scanf("%d",&A[i]);
    }

    int small = 100;
    int small_pos;
    int swap;

    for(j=0;j<n-1;j++){
        for(i=j;i<n;i++){
            if(A[i]<small){
                small=A[i];
                small_pos=i;
            }
        }
        swap=A[j];
        A[j]=A[small_pos];
        A[small_pos]=swap;
        small=100;
    }

    for(i=0;i<n;i++)
        printf("A[%d]=%d\n",i,A[i]);
    return 0;
}

