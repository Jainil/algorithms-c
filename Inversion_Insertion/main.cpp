/* 
 * File:   main.cpp-Inversions
 * Author: pratham
 *
 * Created on 14 August, 2011, 7:09 PM
 */

#include <stdlib.h>
#include <stdio.h>

using namespace std;

/*
 *
 */
int merge(int A[100],int p,int q,int r,int inv){
    int n1 = q-p+1;
    int n2 = r-q;
    int L[n1+1],R[n2+1],i,j,k;
    for(i=0;i<n1;i++)
        L[i]=A[p+i];
    for(i=0;i<n2;i++)
        R[i]=A[q+i+1];
    L[n1]=100;
    R[n2]=100;

    i=0;
    j=0;

    for(j=0;j<n2;){
        if (L[i]>R[j]){
            inv=inv+n1-i;
            j++;
        }
        if (L[i]<=R[j])
            i++;
    }

    i=0;
    j=0;

    for (k=p;k<=r;k++){
        if (L[i]<R[j]){
            A[k]=L[i];
            i++;
        }
        else{
            A[k]=R[j];
            j++;
        }
    }
    printf("%d",inv);
    return(inv);
}

int merge_sort(int A[100], int p, int r, int inv){
    if(p<r){
        int q;
        q = (r+p)/2;
        inv = merge_sort(A,p,q,inv);
        inv = merge_sort(A,q+1,r,inv);
        inv = merge(A,p,q,r,inv);
    }
    return(inv);
}

int main(){
    int n,p,r,inv=0;
    printf("No. of elements in array:\n");
    scanf("%d",&n);
    int A[n],i;
    for (i=0;i<n;i++){
        printf("A[%d]=",i);
        scanf("%d",&A[i]);
//        A[i]=rand()%100+1;
//        printf("A[%d]=%d\n",i,A[i]);
    }
    p=0;
    r=n-1;
    inv = merge_sort(A,p,r,inv);
    for (i=0;i<n;i++){
        printf("A[%d]=%d\n",i,A[i]);
    }
    printf("No. of inversions are %d\n",inv);
}
