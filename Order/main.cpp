/* 
 * File:   main.cpp
 * Author: pratham
 *
 * Created on November 21, 2011, 12:58 AM
 */

#include <stdio.h>
#include <stdlib.h>

using namespace std;

/*
 * 
 */

int n;

void Print(unsigned long s[][10],int i, int j){
    if(i==j)
        printf("A[%d]",i);
    else{
        printf("(");
        Print(s,i,s[i][j]);
        Print(s,s[i][j]+1,j);
        printf(")");
    }
}

int main(int argc, char** argv) {
    int i, j, k, l, q, r;
    do {
        printf("Enter number of matrices:\n");
        scanf("%d", &n);
        int p[n];
        unsigned long m[10][10] = {0}, s[10][10] = {0};
        printf("Enter the dimensional array for the matrices\n");
        for (i = 0; i <= n; i++) {
            scanf("%d", &p[i]);
        }
        for (i = 1; i <= n; i++)
            m[i][i] = 0;
        for (l = 2; l <= n; l++) {
            for (i = 1; i <= (n - l + 1); i++) {
                j = i + l - 1;
                m[i][j] = 99999;
                for (k = i; k <= (j - 1); k++) {
                    q = m[i][k] + m[k + 1][j] + p[i - 1] * p[k] * p[j];
                    if (q < m[i][j]) {
                        m[i][j] = q;
                        s[i][j] = k;
                        //                    printf("s[%d][%d] = %d\n",i,j,s[i][j]);
                        //                    printf("m[%d][%d] = %d\n",i,j,m[i][j]);

                    }
                }
            }
        }
        printf("The correct order is\n");
        Print(s, 1, n);
        printf("\nThe No. of multiplication required is : %d\n", m[1][n]);
        printf("Enter 1:Repeat, 0:Exit\n");
        scanf("%d",&r);
    } while (r == 1);
}

