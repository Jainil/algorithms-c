/* 
 * File:   main.cpp
 * Author: pratham
 *
 * Created on 10 September, 2011, 8:10 PM
 */

#include <stdio.h>
#include <stdlib.h>


/*
 * 
 */

int Parent(int i){
    return (i-1)/2;
}

int LChild(int i){
    return (2*i + 1);
}

int RChild(int i){
    return (2*i + 2);
}

void MaxHeapify(int A[100], int i, int *size){
    int l,r,largest,temp;
    l = LChild(i);
    r = RChild(i);
    if(l<=*size && A[l]>A[i])
        largest = l;
    else largest = i;
    if(r<=*size && A[r]>A[largest])
        largest = r;
    if (largest !=i){
        temp = A[i];
        A[i] = A[largest];
        A[largest] = temp;
        MaxHeapify(A,largest,size);
    }
}

//void Build(int A[100], int length){
//    int i,*size;
//    *size = length-1;
//    printf("size = %d",*size);
//    for(i=(length-1)/2;i>=0;i--)
//        MaxHeapify(A,i,size);
//}

void show (int A[100], int n){
    int i;
    for(i=0;i<n;i++){
        printf("A[%d]=%d\n",i,A[i]);
    }
}

int main() {
    int length,m;
    int* size;
    size=&m;
    printf("Enter number of elements in array\n");
    scanf("%d",&length);
    int A[length],i,temp;
    for(i=0;i<length;i++){
        printf("A[%d]=",i);
        scanf("%d",&A[i]);
    }
    *size = length-1;
    for(i=(length-1)/2;i>=0;i--)
        MaxHeapify(A,i,size);
    
//    Build(A,length);
    for(i=length-1;i>=1;i--){
        temp = A[0];
        A[0] = A[i];
        A[i] = temp;
        *size = *size - 1;
        MaxHeapify(A,0,size);
    }
    printf("Sorted Array:\n");
    show(A,length);
}

