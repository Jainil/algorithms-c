/*
 * File:   main.cpp
 * Author: pratham
 *
 * Created on 25 August, 2011, 12:39 AM
 */

#include <stdio.h>

using namespace std;

/*
 *
 */

struct stack{
    int top;
    int A[100];
};

int push(struct stack *S,int x, int max){
    S->top++;
    S->A[S->top]=x;
}

int pop(struct stack *S){
    S->top--;
    return S->A[S->top+1];
}

int show(struct stack S){
    int i;
    for (i=0;i<=S.top;i++)
        printf("S.A[%d]=%d\n",i,S.A[i]);
}

int main(int argc, char** argv) {
    struct stack S;
    S.top=-1;
    int max;
    printf("enter max number of elements in stack\n");
    scanf("%d",&max);

    int i,x;
    do{
        printf("Enter 1:PUSH, 2:POP, 0:EXIT\n");
        scanf("%d",&i);
        switch(i){
            case 1:
                if (S.top==max-1){
                    printf("Stack OverFlow\n");
                    break;
                }
                printf("Enter value to Insert\n");
                scanf("%d",&x);
                push(&S,x,max);
                show(S);
                break;
            case 2:
                if(S.top==-1){
                    printf("Stack UnderFlow\n");
                    break;
                }
                pop(&S);
                show(S);
                break;
            case 0:
                break;
            default:printf("choice invalid\n");
        }
    }while(i!=0);
    return 0;
}