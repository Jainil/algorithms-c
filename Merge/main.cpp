/* 
 * File:   main.cpp
 * Author: pratham
 *
 * Created on 10 August, 2011, 11:40 PM
 */

#include <stdlib.h>
#include <stdio.h>

using namespace std;

/*
 * 
 */
int merge(int A[],int p,int q,int r){
//   A=(int*)malloc(9*sizeof(int));
    int n1 = q-p+1;
    int n2 = r-q;
    int L[n1+1],R[n2+1],i,j,k;
    for(i=0;i<n1;i++)
        L[i]=A[p+i];
    for(i=0;i<n2;i++)
        R[i]=A[q+i+1];
    L[n1]=100;
    R[n2]=100;

    i=0;
    j=0;

    for (k=p;k<=r;k++){
        if (L[i]<=R[j]){
            A[k]=L[i];
            i++;
        }
        else{
            A[k]=R[j];
            j++;
        }
    }
    return 0;
}


int main() {
    int A[9],p,q,r,i;
    A[0]=40;
    A[1]=17;
    A[2]=18;
    A[3]=21;
    A[4]=30;
    A[5]=19;
    A[6]=20;
    A[7]=32;
    A[8]=6;

    p=1;
    q=4;
    r=7;
    merge(A,p,q,r);
    for(i=0;i<9;i++)
        printf("A[%d]=%d\n",i,A[i]);
}


