/* 
 * File:   main.cpp
 * Author: pratham
 *
 * Created on 10 September, 2011, 6:53 PM
 */

#include <stdio.h>

/*
 * 
 */



int Partition(int A[100], int p, int r){
    int x,i,j,temp;
    x = A[r];
    i = p-1;
    for(j=p;j<=r-1;j++){
        if(A[j]<=x){
            i++;
            temp = A[i];
            A[i] = A[j];
            A[j] = temp;
        }
    }
    temp = A[i+1];
    A[i+1] = A[j];
    A[j] = temp;
    return (i+1);
}

int QuickSort(int A[100], int p, int r){
    int q;
    if(p<r){
        q = Partition(A,p,r);
        QuickSort(A,p,q-1);
        QuickSort(A,q+1,r);
    }
}


void show (int A[100], int n){
    int i;
    for(i=0;i<n;i++){
        printf("A[%d]=%d\n",i,A[i]);
    }
}

int main(int argc, char** argv) {
    int n;
    printf("Enter number of elements in array\n");
    scanf("%d",&n);
    int A[n],i;
    for(i=0;i<n;i++){
        printf("A[%d]=",i);
        scanf("%d",&A[i]);
    }
    QuickSort(A,0,n-1);
    show(A,n);
    return 0;
}

