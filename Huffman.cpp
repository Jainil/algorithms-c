/* 
 * File:   main.cpp
 * Author: pratham
 *
 * Created on November 21, 2011, 10:29 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */

struct node {
    char c;
    int f;
    struct node *lchild, *rchild;
};

int Parent(int i) {
    return (i - 1) / 2;
}

int LChild(int i) {
    return (2 * i + 1);
}

int RChild(int i) {
    return (2 * i + 2);
}

void MinHeapify(struct node *A[100], int i, int *size) {
    int l, r, smallest;
    struct node *temp;
    l = LChild(i);
    r = RChild(i);
    if (l <= *size && A[l]->f < A[i]->f)
        smallest = l;
    else smallest = i;
    if (r <= *size && A[r]->f < A[smallest]->f)
        smallest = r;
    if (smallest != i) {
        temp = A[i];
        A[i] = A[smallest];
        A[smallest] = temp;
        MinHeapify(A, smallest, size);
    }
}

struct node* Extract_Min(struct node *A[10], int *size) {
    struct node *min;
    min = A[0];
    A[0] = A[*size];
    *size = *size - 1;
    MinHeapify(A, 0, size);
    return min;
}

void Traverse(struct node *root, int len, int code[]) {
    int i, lcode[100], rcode[100];
    if (root != NULL) {
        if (root->lchild != NULL) {
            for (i = 0; i < len; i++) {
                lcode[i] = code[i];
            }
            lcode[len] = 0;
            Traverse(root->lchild, len + 1, lcode);
        }
        if (root->rchild != NULL) {
            for (i = 0; i < len; i++) {
                rcode[i] = code[i];
            }
            rcode[len] = 1;
            Traverse(root->rchild, len + 1, rcode);
        }
        if (root->lchild == NULL && root->rchild == NULL) {
            printf("%s - ", &root->c);
            for (i = 0; i < len; i++)
                printf("%d", code[i]);
            printf("\n");
        }
    }
}

int main() {
    int length, m, o;
    int* size = &m;
    printf("Enter number of characters\n");
    scanf("%d", &length);
    *size = length - 1;
    int i, j;
    struct node * A[length];
    for (i = 0; i < length; i++) {
        A[i] = (struct node *) malloc(sizeof (struct node));
        A[i]->lchild = NULL;
        A[i]->rchild = NULL;
    }
    struct node *temp;
    printf("Enter the characters\n");
    for (i = 0; i < length; i++) {
        scanf("%s", &A[i]->c);
    }
    printf("Enter the corresponding frequencies\n");
    for (i = 0; i < length; i++) {
        scanf("%d", &A[i]->f);
    }
    for (i = (length - 1) / 2; i >= 0; i--)
        MinHeapify(A, i, size);

    for (i = 0; i < (length - 1); i++) {
        struct node *z;
        z->lchild = Extract_Min(A, size);
        z->rchild = Extract_Min(A, size);
        z->f = z->lchild->f + z->rchild->f;
        z->c = 'p';
        *size = *size + 1;
        A[*size] = (struct node *) malloc(sizeof (struct node));
        A[*size]->c = z->c;
        A[*size]->f = z->f;
        A[*size]->lchild = z->lchild;
        A[*size]->rchild = z->rchild;
        j = *size;
        while (j > 0 && A[Parent(j)]->f > A[j]->f) {
            temp = A[j];
            A[j] = A[Parent(j)];
            A[Parent(j)] = temp;
            j = Parent(j);
        }
    }
    int code[100];
    Traverse(A[0], 0, code);
}