/*
 * File:   main.cpp
 * Author: pratham
 *
 * Created on 26 August, 2011, 1:53 PM
 */

#include <stdio.h>

using namespace std;

/*
 *
 */

struct queue{
    int head;
    int tail;
    int length;
    int A[100];
};

int enqueue(struct queue *Q, int x, int *no){
    (*no)++;
    Q->A[Q->tail]=x;
    Q->tail = (Q->tail + 1)%Q->length;
//    printf("head = %d,tail = %d\n",Q->head,Q->tail);
}

int dequeue(struct queue *Q, int x, int *no){
    (*no)--;
    x = Q->A[Q->head];
    Q->head = (Q->head + 1)%Q->length;
//    printf("head = %d, tail = %d\n",Q->head,Q->tail);
    return x;
}

int show(struct queue Q,int no){
    int i;
    if (Q.tail>Q.head){
        for(i=Q.head;i<Q.tail;i++)
            printf("A[%d]=%d\n",i,Q.A[i]);
    }
    else if (Q.head>=Q.tail && no!=0){
        for(i=Q.head;i<Q.length;i++)
            printf("A[%d]=%d\n",i,Q.A[i]);
        for(i=0;i<Q.tail;i++)
            printf("A[%d]=%d\n",i,Q.A[i]);
    }
}

int main(int argc, char** argv) {
    struct queue Q;
    Q.head=0;
    Q.tail=0;
    int max;
    printf("enter max number of elements in queue\n");
    scanf("%d",&max);
    Q.length = max;

    int i,x,no = 0;
    do{
        printf("Enter 1:ENQUEUE, 2:DEQUEUE, 0:EXIT\n");
        scanf("%d",&i);
        switch(i){
            case 1:
                if(no == Q.length)
                    printf("Queue Overflows\n");
                else {
                    printf("Enter Element to Enqueue\n");
                    scanf("%d",&x);
                    enqueue(&Q,x,&no);
                    show(Q,no);
//                    printf("%d\n",no);
                }
                break;
            case 2:
                if(no == 0)
                    printf("Queue Underflows\n");
                else {
                    x = dequeue(&Q,x,&no);
                    printf("Dequeued element = %d\n",x);
                    show(Q,no);
//                    printf("%d\n",no);
                }
                break;
            case 0:
                break;
            default:
                printf("choice invalid\n");
        }
    }while(i!=0);
    return 0;
}
