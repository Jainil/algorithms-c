/*
 * File:   main.cpp
 * Author: pratham
 *
 * Created on August 28, 2011, 11:18 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 *
 */

struct list{
    int item;
    struct list *next;
    struct list *prev;
};

struct list* make(int x){
    struct list *temp;
    temp = (struct list *)malloc(sizeof(struct list));
    temp->item = x;
    temp->prev = NULL;
    temp->next = NULL;
    return temp;
};

struct list* add(struct list *head, struct list *new1){
    struct list *temp;
    temp = head;
    if(temp == NULL){
        head = new1;
        return head;
    }
    else if (head->item >= new1->item){
        head = new1;
        new1->next = temp;
        temp->prev = new1;
        return head;
    }
    else{
        while((temp->next != NULL) && (temp->next->item < new1->item))
            temp = temp->next;
        new1->next = temp->next;
        temp->next = new1;
        new1->prev = temp;
        return head;
    }
}

struct list* search(struct list *head, int x){
    struct list *temp;
    temp = head;
    while(temp->next != NULL && temp->item != x)
        temp = temp->next;
    if(temp->item == x){
        printf("Element Found\n");
        return temp;
    }
    else{
        printf("Element not found\n");
        return NULL;
    }
}

struct list* remove(struct  list *head, int x){
    struct list *temp;
    temp = head;
    while((temp->next != NULL) && (temp->item < x))
        temp = temp->next;
    if(temp->item == x){
        if(temp->prev == NULL){
            head = temp->next;
            if(temp->next != NULL)
                head->prev = NULL;
            return head;
        }
        else if(temp->next == NULL){
            (temp->prev)->next = NULL;
            return head;
        }
        else{
            temp->prev->next = temp->next;
            temp->next->prev = temp->prev;
            return head;
        }
    }
    else{
        printf("Element not found\n");
        return head;
    }
}

void show(struct list *head){
    struct list *temp;
    temp = head;
    if (head == NULL)
        printf("List Empty");
    while(temp!=NULL){
        printf("%d\t",temp->item);
        temp = temp->next;
    }
    printf("\n");
}

int main(int argc, char** argv) {
    struct list *head = NULL;
    struct list *new1;
    int i,x;
    do{
        printf("Enter 1:add, 2:delete,  3:search, 0:exit\n");
        scanf("%d",&i);
        switch (i){
            case 1:
                printf("enter item to add\n");
                scanf("%d",&x);
                new1 = make(x);
                head = add(head,new1);
                show(head);
                break;
            case 2:
                if(head == NULL){
                    printf("List is Empty\n");
                }
                else{
                    printf("enter item to delete\n");
                    scanf("%d",&x);
                    head = remove(head,x);
                    show(head);
                }
                break;
            case 3:
                if(head == NULL){
                    printf("List is Empty\n");
                }
                else{
                    printf("enter item to search\n");
                    scanf("%d",&x);
                    search(head,x);
                    show(head);
                }
                break;
            case 0:
                break;
            default:
                printf("Invalid Selection\n");
        }
    }while(i!=0);
    return 0;
}
