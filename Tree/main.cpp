/*
 * File:   main.cpp
 * Author: pratham
 *
 * Created on August 29, 2011, 8:32 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 *
 */

struct tree{
    int item;
    struct tree *parent;
    struct tree *lchild;
    struct tree *rchild;
};

struct tree* make(int x){
    struct tree *temp;
    temp = (struct tree *)malloc(sizeof(struct tree));
    temp->item = x;
    temp->parent = NULL;
    temp->lchild = NULL;
    temp->rchild = NULL;
    return temp;
}

struct tree* insert(struct tree *head){
    struct tree *temp = head,*new1;
    int z,x;
    if(head->lchild != NULL && head->rchild != NULL){
        printf("Error : at this node both left and right child present\n");
        return head;
    }
    else{
        printf("Enter 1:insert at left child, 2:insert at right child\n");
        scanf("%d",&z);
        if(z == 1){
            if(head->lchild != NULL){
                printf("Error : left child already present");
                return head;
            }
            else{
                printf("Enter Element to add\n");
                scanf ("%d",&x);
                new1 = make(x);
                new1->parent = head;
                head->lchild = new1;
                head = new1;
                return head;
            }
        }
        else if(z == 2){
            if(head->rchild != NULL){
                printf("Error : right child child already present");
                return head;
            }
            else{
                printf("Enter Element to add\n");
                scanf ("%d",&x);
                new1 = make(x);
                new1->parent = head;
                head->rchild = new1;
                head = new1;
                return head;
            }
        }
    }

}

struct tree* traverse(struct tree *head){
    int j;
    do{
        printf("You are at element = %d\n",head->item);
        printf("Enter 1:Goto Parent, 2:Goto Left Child, 3:Goto Right Child, 0:Exit traversing\n");
        scanf("%d",&j);
        switch(j){
            case 1:
                if(head->parent == NULL){
                    printf("Node parent is NULL\n");
                    break;
                }
                else{
                    head = head->parent;
                    break;
                }
            case 2:
                if(head->lchild == NULL){
                    printf("Node Left Child is NULL\n");
                    break;
                }
                else{
                    head = head->lchild;
                    break;
                }
            case 3:
                if(head->rchild == NULL){
                    printf("Node Right Child is NULL\n");
                    break;
                }
                else{
                    head = head->rchild;
                    break;
                }
            case 0:
                return head;
            default:
                printf("Invalid Selection");
        }
    }while(j!=0);
}

void printout(struct tree *root){
    if(root == NULL){
        printf("No tree\n");
    }
    else{
        if(root->lchild != NULL)
            printout(root->lchild);
        printf("%d\n", root->item);
        if(root->rchild != NULL)
            printout(root->rchild);
    }
}

int main(int argc, char** argv) {
    struct tree *root,*new1,*head;
    root = NULL;
    head = root;
    int i,x;
    do{
        printf("Enter 1:add, 2:traverse, 3:show tree, 0:exit\n");
        scanf("%d",&i);
        switch (i){
            case 1:
                if(root == NULL){
                    printf("Enter Element to add\n");
                    scanf ("%d",&x);
                    new1 = make(x);
                    head = new1;
                    root = new1;
                    break;
                }
                else {
                    printf("You are at element = %d\n",head->item);
                    head = insert(head);
                    break;
                }
            case 2:
                if(root == NULL){
                    printf("Error: No Tree\n");
                    break;
                }
                else{
                head = traverse(head);
                break;
                }
            case 3:
                printf("Entered tree : \n");
                printout(root);
                break;
            case 0:
                break;
            default:
                printf("Invalid Selection\n");
        }
    }while(i!=0);
    return 0;
}
