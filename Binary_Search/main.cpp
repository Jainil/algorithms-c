/* 
 * File:   main.cpp
 * Author: pratham
 *
 * Created oCppApplication_1n 14 August, 2011, 1:16 PM
 */


#include <stdlib.h>
#include <stdio.h>

using namespace std;

/*
 *
 */
int merge(int A[15],int p,int q,int r){
    int n1 = q-p+1;
    int n2 = r-q;
    int L[n1+1],R[n2+1],i,j,k;
    for(i=0;i<n1;i++)
        L[i]=A[p+i];
    for(i=0;i<n2;i++)
        R[i]=A[q+i+1];
    L[n1]=100;
    R[n2]=100;

    i=0;
    j=0;

    for (k=p;k<=r;k++){
        if (L[i]<=R[j]){
            A[k]=L[i];
            i++;
        }
        else{
            A[k]=R[j];
            j++;
        }
    }
    return 0;
}

int merge_sort(int A[15], int p, int r){
    if(p<r){
        int q;
        q = (r+p)/2;
        merge_sort(A,p,q);
        merge_sort(A,q+1,r);
        merge(A,p,q,r);
    }
}

int binary(int A[15], int v, int start, int end){
    int n = (end-start)/2;
    n = start + n;
    if(start>end)
        return(16);
    else if (v<A[n]){
        end = n-1;   
        binary(A,v,start,end);
    }
    else if (v>A[n]){
        start = n+1;        
        binary(A,v,start,end);
    }
    else if (v=A[n]){
        return(n);
    }
 }

int main(){
    int n = 15;
    int A[n],i,p,r;
    for (i=0;i<n;i++){
        A[i]=rand()%30+1;
        printf("A[%d]=%d\n",i,A[i]);
    }
    p=0;
    r=n-1;
    merge_sort(A,p,r);
    for (i=0;i<n;i++)
        printf("A[%d]=%d\n",i,A[i]);

    int v;
    printf("enter a number between 1 and 30\n");
    scanf("%d",&v);
    int start = 0, end = n-1;
    int ans;
    ans = binary(A,v,start,end);
    if(ans==16)
        printf("Position is not found. There is no such number\n");
    else
        printf("Position of %d is %d\n",v,ans);
}


