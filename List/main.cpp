/*
 * File:   main.cpp
 * Author: pratham
 *
 * Created on August 27, 2011, 11:15 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 *
 */

struct list{
    int item;
    struct list *next;
};


struct list* make(int x){
    struct list *temp;
    temp = (struct list *)malloc(sizeof(struct list));
    temp->next = NULL;
    temp->item = x;
    return temp;
}

struct list* add(struct list *head, struct list *new1){
    struct list *temp;
    temp = (struct list *)malloc(sizeof(struct list));
    temp = head;
    if(head == NULL){
        head = new1;
        return head;
    }
    if(head->item >= new1->item){
        new1->next = temp;
        head = new1;
        return head;
    }
    else{
        while((temp->next != NULL) && (temp->next->item < new1->item))
            temp = temp->next;
        new1->next = temp->next;
        temp->next = new1;
        return head;
    }
}

struct list* search(struct list *head, int x){
    struct list *temp;
    temp = head;
    while(temp->next != NULL && temp->item != x)
        temp = temp->next;
    if(temp->item == x){
        printf("Element Found\n");
        return temp;
    }
    else{
        printf("Element not found\n");
        return NULL;
    }
}

struct list* remove(struct list *head, int x){
    struct list *temp;
    temp = head;
    if(head->item == x){
        head = temp->next;
        return head;
    }
    while((temp->next != NULL) && (temp->next->item != x))
        temp = temp->next;
    if(temp->next == NULL){
        printf("Element not Found\n");
        return head;
    }
    else if(temp->next->item == x){
        temp->next = temp->next->next;
        return head;
    }
}

void show(struct list *head){
    struct list *temp;
    temp = head;
    if (head == NULL)
        printf("List Empty");
    while(temp!=NULL){
        printf("%d\t",temp->item);
        temp = temp->next;
    }
    printf("\n");
}

int main(int argc, char** argv) {
    struct list *head = NULL;
    struct list *new1;
    int i,x;
    do{
        printf("Enter 1:add, 2:delete,  3:search, 0:exit\n");
        scanf("%d",&i);
        switch (i){
            case 1:
                printf("enter item to add\n");
                scanf("%d",&x);
                new1 = make(x);
                head = add(head,new1);
                show(head);
                break;
            case 2:
                if(head == NULL){
                    printf("Error:List is Empty\n");
                }
                else{
                    printf("enter item to delete\n");
                    scanf("%d",&x);
                    head = remove(head,x);
                    show(head);
                }
                break;
            case 3:
                if(head == NULL){
                    printf("List is Empty\n");
                }
                else{
                    printf("enter item to search\n");
                    scanf("%d",&x);
                    search(head,x);
                    show(head);
                }
                break;
            case 0:
                break;
            default:
                printf("Invalid Selection\n");
        }
    }while(i!=0);
    return 0;
}
