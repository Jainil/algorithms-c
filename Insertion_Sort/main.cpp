/* 
 * File:   main.cpp
 * Author: pratham
 *
 * Created on 10 August, 2011, 10:59 PM
 */

#include <stdio.h>
#include <stdlib.h>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int A[10],i,j,key;
    for (i=0;i<10;i++){
         A[i] = rand()%100 + 1;
         printf("A[%d] = %d\n",i,A[i]);
    }

    for (j=1;j<10;j++){
        key = A[j];
        for (i=j-1;i>=0 && A[i]>=key;i--)
            A[i+1]=A[i];
        A[i+1]=key;

    }
    for (i=0;i<10;i++){
         printf("A[%d] = %d\n",i,A[i]);
    }   
}

