/* 
 * File:   main.cpp
 * Author: pratham
 *
 * Created on October 18, 2011, 6:02 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 *
 */

struct tree {
    int item;
    struct tree *parent;
    struct tree *lchild;
    struct tree *rchild;
};

struct tree* make(int x) {
    struct tree *temp;
    temp = (struct tree *) malloc(sizeof (struct tree));
    temp->item = x;
    temp->parent = NULL;
    temp->lchild = NULL;
    temp->rchild = NULL;
    return temp;
}

struct tree* Tree_Insert(struct tree *root) {
    struct tree *x, *y, *z;
    int key;
    printf("Enter Key to Insert\n");
    scanf("%d", &key);
    z = make(key);
    y = NULL;
    x = root;
    while (x != NULL) {
        y = x;
        if (z->item < x->item)
            x = x->lchild;
        else
            x = x->rchild;
    }
    z->parent = y;
    if (y == NULL)
        root = z;
    else if (z->item < y->item)
        y->lchild = z;
    else
        y->rchild = z;
    return root;
}

struct tree* Search(struct tree *head, int key) {
    if (head == NULL || key == head->item)
        return head;
    if (key < head->item)
        return Search(head->lchild, key);
    else
        return Search(head->rchild, key);
}

struct tree* Min(struct tree *head) {
    while (head->lchild != NULL)
        head = head->lchild;
    return head;
}

struct tree* Transplant(struct tree *root, struct tree *u, struct tree *v) {
    if (u->parent == NULL)
        root = v;
    else if (u == u->parent->lchild)
        u->parent->lchild = v;
    else u->parent->rchild = v;
    if (v != NULL)
        v->parent = u->parent;
    return root;
}

struct tree* Delete(struct tree *root, struct tree *z) {
    struct tree *y;
    if (z->lchild == NULL)
        root = Transplant(root, z, z->rchild);
    else if (z->rchild == NULL)
        root = Transplant(root, z, z->lchild);
    else {
        y = Min(z->rchild);
        if (y->parent != z) {
            root = Transplant(root, y, y->rchild);
            y->rchild = z->rchild;
            y->rchild->parent = y;
        }
        root = Transplant(root, z, y);
        y->lchild = z->lchild;
        y->lchild->parent = y;
    }
    return root;
}

void Traverse(struct tree *head) {
    if (head != NULL) {
        Traverse(head->lchild);
        printf("%d\n", head->item);
        Traverse(head->rchild);
    }
}

int main(int argc, char** argv) {
    struct tree *root, *new1, *head;
    root = NULL;
    int i, x;
    do {
        printf("Enter 1:Insert, 2:Delete, 3:Search, 4:Inorder Traverse, 0:exit\n");
        scanf("%d", &i);
        switch (i) {
            case 1:
                root = Tree_Insert(root);
                break;
            case 2:
                if (root == NULL)
                    printf("Tree is Empty\n");
                else {
                    printf("Enter Key to Delete\n");
                    scanf("%d", &x);
                    head = Search(root, x);
                    if (head != NULL)
                        root = Delete(root, head);
                    else
                        printf("Key Not Found\n");
                }
                break;
            case 3:
                if (root == NULL)
                    printf("Tree is Empty\n");
                else {
                    printf("Enter Key to Search\n");
                    scanf("%d", &x);
                    head = Search(root, x);
                    if (head != NULL)
                        printf("Key Found\n");
                    else
                        printf("Key Not Found\n");
                }
                break;
            case 4:
                if (root == NULL)
                    printf("Tree is Empty\n");
                else {
                    printf("Inorder Traversal of Tree\n");
                    Traverse(root);
                }
                break;
            case 0:
                break;
            default:
                printf("Invalid Selection\n");
        }
    } while (i != 0);
    return 0;
}

