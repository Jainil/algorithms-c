/* 
 * File:   main.cpp
 * Author: pratham
 *
 * Created on 10 September, 2011, 8:10 PM
 */

#include <stdio.h>
#include <stdlib.h>


/*
 * 
 */

int Parent(int i){
    return (i-1)/2;
}

int LChild(int i){
    return (2*i + 1);
}

int RChild(int i){
    return (2*i + 2);
}

void MaxHeapify(int A[100], int i, int *size){
    int l,r,largest,temp;
    l = LChild(i);
    r = RChild(i);
    if(l<=*size && A[l]>A[i])
        largest = l;
    else largest = i;
    if(r<=*size && A[r]>A[largest])
        largest = r;
    if (largest !=i){
        temp = A[i];
        A[i] = A[largest];
        A[largest] = temp;
        MaxHeapify(A,largest,size);
    }
}

void IncreaseKey(int A[100], int i, int key){
    int temp;
    if(key<A[i])
	printf("Error : New Key is smaller than current key\n");
    else{
	A[i] = key;
	while(i>0 && A[Parent(i)]<A[i]){
	    temp = A[i];
	    A[i] = A[Parent(i)];
	    A[Parent(i)] = temp;
	    i = Parent(i);
	}
    }
}

void show (int A[100], int n){
    int i;
    for(i=0;i<n;i++){
        printf("A[%d]=%d\n",i,A[i]);
    }
}

int main() {
    int length,m;
    int* size;
    size=&m;
    printf("Enter number of elements in array\n");
    scanf("%d",&length);
    int A[100],i,max,key,id;
    for(i=0;i<length;i++){
        printf("A[%d]=",i);
        scanf("%d",&A[i]);
    }
    *size = length-1;
    for(i=(length-1)/2;i>=0;i--)
        MaxHeapify(A,i,size);

    do{
	printf("Enter 1:Extract Max, 2:Increase Key, 3:Insert, 4:Show, 0:Exit\n");
	scanf("%d",&i);
	switch (i){
            case 1:
                if(*size<0)
		    printf("Error : Heap Underflow\n");
		max = A[0];
		A[0] = A[*size];
		*size = *size - 1;
		MaxHeapify(A,0,size);
		printf("Extracted Element is %d\n",max);
		length = length - 1;
		break;
            case 2:
                printf("Enter the index\n");
		scanf("%d",&id);
		if(id<0 || id>length-1)
		    printf("Error : index not present\n");
		else{
		    printf("New key of the element\n");
		    scanf("%d",&key);
		    IncreaseKey(A,id,key);
		}
		break;
            case 3:
		printf("Enter Key to Insert\n");
		scanf("%d",&key);
		*size = *size + 1;
		A[*size] = -9;
		IncreaseKey(A,*size,key);
		length = length + 1;
		break;
	    case 4:
		show(A,length);
		break;        
            case 0:
                show(A,length);
		break;
            default:
                printf("Invalid Selection\n");
        }
    }while(i!=0);	
}

