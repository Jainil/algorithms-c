/* 
 * File:   main.cpp-Insertion in Merge
 * Author: pratham
 *
 * Created on 18 August, 2011, 4:56 PM
 */

#include <stdlib.h>
#include <stdio.h>

using namespace std;

/*
 *
 */
int insertion(int A[12],int low, int up){
    int i,j,key;
    for (j=low+1;j<=up;j++){
        key = A[j];
        for (i=j-1;i>=low && A[i]>=key;i--)
            A[i+1]=A[i];
        A[i+1]=key;
    }
}

int merge(int A[],int p,int q,int r){
    int n1 = q-p+1;
    int n2 = r-q;
    int L[n1+1],R[n2+1],i,j,k;
    for(i=0;i<n1;i++)
        L[i]=A[p+i];
    for(i=0;i<n2;i++)
        R[i]=A[q+i+1];
    L[n1]=100;
    R[n2]=100;

    i=0;
    j=0;

    for (k=p;k<=r;k++){
        if (L[i]<=R[j]){
            A[k]=L[i];
            i++;
        }
        else{
            A[k]=R[j];
            j++;
        }
    }
    return 0;
}

int main(){
    int n,k,i;
    while (i==0){
        printf("Length of array:\n");
        scanf("%d",&n);
        printf("Length of sublists:\n");
        scanf("%d",&k);
        if(n%k==0)
            i=1;
        if(n%k!=0)
            printf("Error : No. of sublists not an integer");
    }
    int A[n];
    for (i=0;i<n;i++){
        printf("A[%d]=",i);
        scanf("%d",&A[i]);

    }

    int up,low=0;

    for(i=0;i<n/k;i++){
        up = low + k - 1;
        insertion(A,low,up);
        low = low + k;
    }

    int p = 0,q = k-1,r = 2*k-1;

    for(i=1;i<n/k;i++){
        merge(A,p,q,r);
        q = q+k;
        r = r+k;
    }

    for(i=0;i<n;i++)
        printf("A[%d]=%d\n",i,A[i]);
}